from fastapi.testclient import TestClient
from .session import router
import mongomock
from ...database.mongod_connection import get_database
from ...services.session_service import SESSION_COLLECTION

client = TestClient(router)

@mongomock.patch(servers=(("mongod", 27017),))
def test_post_create_session():
    database = get_database()
    session_collection = database[SESSION_COLLECTION]
    assert session_collection.count_documents({}) == 0

    response = client.post("/session")
    assert response.status_code == 200
    assert session_collection.count_documents({}) == 1

    sessions = session_collection.find()
    session = sessions[0]
    session["_id"] = str(session["_id"])
    session["expires_at"] = session["expires_at"].isoformat()
    assert response.json() == session
