from fastapi import APIRouter
from ...services.session_service import create_session

router = APIRouter(
    prefix="/session"
)

@router.post('/')
async def post_create_session():
    """Endpoint to create new session
    
    Returns:
    json: JSON version of the Session object created
    """
    session = create_session()
    session_json = session.to_json()
    return session_json
