from .session_service import get_sessions, SESSION_COLLECTION, generate_room_code, CODE_LENGTH, create_session
import mongomock
from ..models.session import Session
from ..database.mongod_connection import get_database

@mongomock.patch(servers=(("mongod", 27017),))
def test_get_sessions():
    database = get_database()
    session_collection = database[SESSION_COLLECTION]
    assert session_collection.count_documents({}) == 0
    sessions = get_sessions()
    assert len(list(sessions)) == 0

    session = Session()
    session_collection.insert_one(session.to_json())
    assert session_collection.count_documents({}) == 1
    sessions = get_sessions()
    assert len(list(sessions)) == 1

def test_generate_room_code():
    code = generate_room_code()
    assert len(code) == CODE_LENGTH
    assert type(code) is str

@mongomock.patch(servers=(("mongod", 27017),))
def test_create_session():
    database = get_database()
    session_collection = database[SESSION_COLLECTION]
    assert session_collection.count_documents({}) == 0

    session = create_session()
    assert type(session) is Session
    mongo_entry = session_collection.find()
    assert Session(mongo_entry[0]) == session

@mongomock.patch(servers=(("mongod", 27017),))
def test_unique_codes():
    for _ in range(100):
        create_session()

    database = get_database()
    session_collection = database[SESSION_COLLECTION]
    codes = set()
    for document in session_collection.find():
        codes.add(document["room_code"])

    assert len(codes) == 100

@mongomock.patch(servers=(("mongod", 27017),))
def test_forced_unique_codes(mocker):
    count = 0

    def mocked_generate_code():
        nonlocal count
        count += 1
        if count < 3:
            return "HSAZ"
        return "KAIX"
    
    mocker.patch("src.services.session_service.generate_room_code", side_effect=mocked_generate_code)
    create_session()
    create_session()

    database = get_database()
    session_collection = database[SESSION_COLLECTION]
    codes = set()
    for document in session_collection.find():
        codes.add(document["room_code"])

    assert len(codes) == 2