from ..database.mongod_connection import get_database
from ..models.session import Session
from random import choices
from string import ascii_uppercase
from datetime import datetime, timedelta

SESSION_COLLECTION = "sessions"
CODE_LENGTH = 4
EXPIRE_MINUTES = 30

def get_sessions():
    """Gets all sessions inside of database
    
    Returns:
    Mongodb Cursor: All found sessions
    """
    database = get_database()
    sessions = database[SESSION_COLLECTION]
    return sessions.find()

def generate_room_code():
    """Generates random code containing letters equal to CODE_LENGTH; all uppercase
    
    Returns:
    str: CODE_LENGTH long uppercase letters 
    """
    random_result = choices(ascii_uppercase, k=CODE_LENGTH)
    return ''.join(random_result)

def create_session():
    """Creates a new session inside of mongodb instance.
    Gurantees uniqueness of room_code.
    Sets expiration to EXPIRE_MINUTES from session creation.

    Returns:
    Session: Newly generated Session object
    """
    database = get_database()
    sessions = database[SESSION_COLLECTION]
    
    code = generate_room_code()
    while sessions.count_documents({"room_code": code, "is_playing": False, "expires_at": {"$gte": datetime.now()}}):
        code = generate_room_code()
    
    expires_at = datetime.now() + timedelta(minutes=EXPIRE_MINUTES)
    session = Session()
    session.expires_at = expires_at.replace(microsecond=0)
    session.room_code = code
    session_json = session.to_json()
    session_json.pop("_id")
    document = sessions.insert_one(session_json)
    session._id = document.inserted_id
    return session