import pymongo

CLIENT_URL = "mongodb://mongod:27017/"
DATABASE_NAME = "anotheranimegame"

def get_client():
    """Returns client for consumption.
    
    Returns:
    MongoClient: The client for accessing CLIENT_URL.
    """
    return pymongo.MongoClient(CLIENT_URL)

def get_database():
    """Returns database for consumption.
    
    Returns:
    Database: The database found at CLIENT_URL with name DATABASE_NAME.
    """
    client = get_client()
    return client[DATABASE_NAME]
