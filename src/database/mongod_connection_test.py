import mongomock
from .mongod_connection import  get_client, get_database

@mongomock.patch(servers=(("mongod", 27017),))
def test_get_client():
    connection = get_client()
    assert hasattr(connection, "port")

@mongomock.patch(servers=(("mongod", 27017),))
def test_get_database():
    schema = get_database()
    assert hasattr(schema, "find") and callable(getattr(schema, "find"))