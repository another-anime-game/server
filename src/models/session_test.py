from .session import Session, PlayerAlreadyInSessionException, PlayerNotInSessionException
from .player import Player
from datetime import datetime

def test_session():
    session = Session()
    assert hasattr(session, "room_code")
    assert hasattr(session, "connected_players")
    assert hasattr(session, "game_config")
    assert hasattr(session, "_id")
    assert hasattr(session, "expires_at")
    assert hasattr(session, "is_playing")

def test_from_json():
    now = datetime.now()
    session_data = {"room_code": "ZFSE", "connected_players": [], "game_config": {}, "_id": "test_id", "expires_at": now, "is_playing": False}
    session = Session(session_data)
    assert hasattr(session, "room_code") and session.room_code == "ZFSE"
    assert hasattr(session, "connected_players") and session.connected_players == []
    assert hasattr(session, "game_config") and session.game_config == {}
    assert hasattr(session, "_id") and session._id == "test_id"
    assert hasattr(session, "expires_at") and session.expires_at == now
    assert hasattr(session, "is_playing") and not session.is_playing

def test_add_player():
    now = datetime.now()
    session_data = {"room_code": "ZFSE", "connected_players": [], "game_config": {}, "_id": "test_id", "expires_at": now, "is_playing": False}
    session = Session(session_data)
    player = Player({"name": "test_player", "_id": "player_id", "score": 0, "room_owner": False})
    assert len(session.connected_players) == 0
    session.add_player(player)
    assert player in session.connected_players

def test_add_player_exception():
    now = datetime.now()
    player_id = "player_id"
    player1 = Player({"name": "test_player", "_id": player_id, "score": 0, "room_owner": False})
    player2 = Player({"name": "test_player", "_id": "player_2", "score": 0, "room_owner": False})
    session_data = {"room_code": "ZFSE", "connected_players": [player2, player1], "game_config": {}, "_id": "test_id", "expires_at": now, "is_playing": False}
    session = Session(session_data)
    try:
        session.add_player(player1)
        assert False
    except PlayerAlreadyInSessionException:
        assert True

def test_remove_player():
    now = datetime.now()
    player_id = "player_id"
    player = Player({"name": "test_player", "_id": player_id, "score": 0, "room_owner": False})
    session_data = {"room_code": "ZFSE", "connected_players": [player], "game_config": {}, "_id": "test_id", "expires_at": now, "is_playing": False}
    session = Session(session_data)
    assert len(session.connected_players) == 1
    session.remove_player(player_id)
    assert player not in session.connected_players

def test_remove_player_exception():
    now = datetime.now()
    player_id = "player_id"
    player = Player({"name": "test_player", "_id": "player", "score": 0, "room_owner": False})
    session_data = {"room_code": "ZFSE", "connected_players": [player], "game_config": {}, "_id": "test_id", "expires_at": now, "is_playing": False}
    session = Session(session_data)
    try:
        session.remove_player(player_id)
        assert False
    except PlayerNotInSessionException:
        assert True

def test_to_json():
    now = datetime.now()
    player_id = "player_id"
    player = Player({"name": "test_player", "_id": player_id, "score": 0, "room_owner": False})
    session_data = {"room_code": "ZFSE", "connected_players": [player], "game_config": {}, "_id": "test_id", "expires_at": now, "is_playing": False}
    session = Session(session_data)
    json_blob = session.to_json()
    session_data["connected_players"] = [player.to_json()]
    assert json_blob == session_data

def test_equality():
    now = datetime.now()
    player_id = "player_id"
    player = Player({"name": "test_player", "_id": player_id, "score": 0, "room_owner": False})
    session_data = {"room_code": "ZFSE", "connected_players": [player], "game_config": {}, "_id": "test_id", "expires_at": now, "is_playing": False}
    session1 = Session(session_data)
    session2 = Session(session_data)
    assert session1 == session2

def test_inequality():
    now = datetime.now()
    player_id = "player_id"
    player = Player({"name": "test_player", "_id": player_id, "score": 0, "room_owner": False})
    session_data = {"room_code": "ZFSE", "connected_players": [player], "game_config": {}, "_id": "test_id", "expires_at": now, "is_playing": False}
    
    session1 = Session(session_data)
    session2 = Session(session_data)
    session1.is_playing = True
    assert session1 != session2

    session1 = Session(session_data)
    session1.game_config = {"type": "something"}
    assert session1 != session2

    session1 = Session(session_data)
    session1.expires_at = None
    assert session1 != session2

    session1 = Session(session_data)
    session1.room_code = "SDFA"
    assert session1 != session2

    session1 = Session(session_data)
    session1.connected_players = []
    assert session1 != session2

    session1 = Session(session_data)
    session1._id = "test_id2"
    assert session1 != session2

def test_equality_not_implimented():
    session = Session()
    player = Player()

    assert session != player