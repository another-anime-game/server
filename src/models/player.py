class Player:
    """Player class to maintain parity between Player objects during runtime."""
    _id = None
    room_owner = False
    name = None
    score = 0

    def __init__(self, player_json = None):
        """Accepts options JSON blob to initalize object with; otherwise blank Player is created."""
        if not player_json:
            return
        self._id = player_json["_id"]
        self.room_owner = player_json["room_owner"]
        self.name = player_json["name"]
        self.score = player_json["score"]

    def to_json(self):
        """Transforms class attributes into JSON format for consumption.
        
        Returns:
        json: JSON version of Player object.
        """
        return {
            "_id": str(self._id),
            "room_owner": self.room_owner,
            "name": self.name,
            "score": self.score
        }
