from .player import Player

def test_player():
    player = Player()
    assert hasattr(player, "_id")
    assert hasattr(player, "name")
    assert hasattr(player, "score")
    assert hasattr(player, "room_owner")

def test_player_from_json():
    player_json = {"_id": "player_id", "name": "test_name", "score": 0, "room_owner": True}
    player = Player(player_json)
    assert hasattr(player, "_id") and player._id == "player_id"
    assert hasattr(player, "name") and player.name == "test_name"
    assert hasattr(player, "score") and player.score == 0
    assert hasattr(player, "room_owner") and player.room_owner

def test_player_to_json():
    player_json = {"_id": "player_id", "name": "test_name", "score": 0, "room_owner": True}
    player = Player(player_json)
    json_blob = player.to_json()
    assert player_json == json_blob