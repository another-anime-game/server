from .player import Player

class Session:
    """Session class to maintain parity between Sessions during creation process."""
    _id = None
    connected_players = []
    expires_at = None
    game_config = None
    is_playing = False
    room_code = None

    def __init__(self, session_json = None):
        """Accepts options JSON blob to initalize object with; otherwise blank Sesssion is created."""
        if not session_json:
            return
        self._id = session_json["_id"]
        self.connected_players = session_json["connected_players"]
        self.expires_at = session_json["expires_at"]
        self.game_config = session_json["game_config"]
        self.is_playing = session_json["is_playing"]
        self.room_code = session_json["room_code"]

    def to_json(self):
        """Transforms class attributes into JSON format for consumption.
        
        Returns:
        json: JSON version of Session object.
        """
        players = []
        for player in self.connected_players:
            players.append(player.to_json())
        
        return {
            "_id": str(self._id),
            "connected_players": players,
            "expires_at": self.expires_at,
            "game_config": self.game_config,
            "is_playing": self.is_playing,
            "room_code": self.room_code
        }
    
    def add_player(self, player: Player):
        """Adds player to Session object while guaranteeing duplicate _id isn't added.
        
        Raises:
        PlayerAlreadyInSessionException: Raises in the case player was already in session.
        """
        for connected_player in self.connected_players:
            if connected_player._id == player._id:
                raise PlayerAlreadyInSessionException
        
        self.connected_players.append(player)

    def remove_player(self, player_id: str):
        """Removes player from session by id.
        
        Parameters:
        player_id (str): The _id to search for within connected_players.

        Raises:
        PlayerNotInSessionException: Raises in the case player wasn't found in session.
        """
        index_to_remove = -1

        for index, connected_player in enumerate(self.connected_players):
            if connected_player._id == player_id:
                index_to_remove = index
                break
        
        if index_to_remove == -1:
            raise PlayerNotInSessionException
        
        self.connected_players.pop(index)

    def __eq__(self, other: object) -> bool:
        """Returns if two Sessions are equal based on serialization.
        
        Returns:
        bool: Whether or not the Sessions are the same.
        """
        if type(other) is not Session:
            return False
        
        return self.to_json() == other.to_json()


class PlayerAlreadyInSessionException(Exception):
    """Raised when a player is already inside of a session"""
    pass

class PlayerNotInSessionException(Exception):
    """Raised when a player is not in session"""
    pass