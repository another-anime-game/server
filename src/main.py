from fastapi import FastAPI
from .routers.v1 import session

app = FastAPI()

app.include_router(session.router, prefix="/api/v1")