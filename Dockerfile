FROM python:3.11-slim-buster as dev
WORKDIR /app
COPY . .
RUN pip3 install -r dev-requirements.txt
EXPOSE 8000
CMD ["python3", "-m", "uvicorn", "src.main:app", "--reload", "--host", "0.0.0.0"]

FROM python:3.11-slim-buster as prod
WORKDIR /app
COPY --from=dev /app .
RUN pip3 install -r requirements.txt
EXPOSE 8000
CMD ["python3", "-m", "uvicorn", "src.main:app", "--reload", "--host", "0.0.0.0"]