# Another Anime Game | Server Repo

This is the repository for all things server related for the Another Anime Game website.

## How to run

For local development, Docker is the desired environment. Preparing your computer to develop is as simple as running `docker compose up --build -d` from the main directory.

## Testing

After running the docker containers in either a separate tab or in detached mode, running the tests is simply `docker compose exec server python3 -m pytest`.

### Coverage

Similarly, in order to get the coverage of the code base, running `docker compose exec server python3 -m pytest --cov ./src` will work. Generating a XML coverage report is nearly the same command: `docker compose exec server python3 -m pytest --cov ./src --cov-report xml:coverage.xml`.